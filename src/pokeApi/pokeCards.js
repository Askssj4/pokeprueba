import React, { useEffect, useState, Fragment } from 'react'
import {
    Card,
    CardBody,
    CardTitle,
    CardSubtitle,
    CardText,
    Label,
    Container,
} from 'reactstrap'
import './pokeApi.css'
const PokeCards = props => {

    const [pokeImage, setPokeImage] = useState(null)
    const [pokeHabilidades, setPokeHabilidades] = useState(null)

    useEffect(() => {
        if (props.poke_imagen != null) {
            setPokeImage(props.poke_imagen)
        }
    }, [props.poke_imagen])

    useEffect(() => {
        if (props.poke_habilidades != null) {
            setPokeHabilidades(props.poke_habilidades)
        }
    }, [props.poke_habilidades])

    useEffect(() => {
        if (pokeHabilidades) {
        }
    }, [pokeHabilidades])

    return (
        <Fragment>
            <img
                style={{ height: '80vh', width: '80vh' }}
                src={pokeImage ? pokeImage : "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Pokebola-pokeball-png-0.png/601px-Pokebola-pokeball-png-0.png"}
            />
                <Card
                    style={{
                        width: '33%',
                        backgroundColor: 'transparent',
                        border: 'none',
                        alignSelf: 'center',
                    }}
                >

                    <CardBody style={{fontFamily: 'fantasy', textShadow: '3px 3px #FFFFFF'}}>
                        <CardTitle tag="h1">
                        Nombre:  <b>{props.poke_nombre}</b>
                        </CardTitle>
                        <CardSubtitle
                            className="mt-10"
                            tag="h2"
                        >
                           <b> Habilidades</b>
                        </CardSubtitle>
                        <CardText>
                            {pokeHabilidades ?
                                pokeHabilidades.map(i => {
                                    return (
                                        <>
                                            <Label style={{fontSize: 30, fontFamily: 'cursive'}}>
                                                <b>{i.ability.name}</b>
                                            </Label>
                                            <br />
                                        </>
                                    )
                                })
                                : ""}
                        </CardText>
                    </CardBody>
                </Card>
        </Fragment>
    )
}

export default PokeCards