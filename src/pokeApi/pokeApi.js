import React, { useEffect, useState, Fragment } from 'react'
import {
    Container,
    Row,
    Col,
    Input,
    ButtonToggle

} from 'reactstrap'
import request from 'superagent';
import PokeCards from './pokeCards';
import './pokeApi.css'
import Swal from 'sweetalert2';
import { FaEye, FaSearch } from 'react-icons/fa';

const PokeApi = props => {
    const [listaPokemon, setListaPokemon] = useState(null)
    const [pokeBusqueda, setPokeBusqueda] = useState(null)

    const obtenerPokemon = async () => {
        try {
            let lista_pokemon = await request.get("https://pokeapi.co/api/v2/pokemon/" + pokeBusqueda + "/")
                .set('Accept', 'application/json')
            console.log("lista_pokemon.body", lista_pokemon.body)
            setListaPokemon(lista_pokemon.body)
        }
        catch (e) {
            console.log("error: ", e);
            Swal.fire({
                title: "Error en obtener el pokemon",
                icon: "error",
                text: "No se ha encontrado al pokemon solicitado",
                button: "Aceptar"
            });
        }
    }

    return (
        <Fragment>
            <div className="page-content">
                <Container fluid={false}
                >
                    <div className='pokeFondo'>
                        <Row>
                            <Col>
                                <div style={{marginLeft: '15px'}}>
                                    <Input
                                        style={{ marginTop: '10px' }}
                                        onChange={e => { setPokeBusqueda(e.target.value.toLowerCase()) }}
                                        placeholder='Ingrese el nombre del pokemon a buscar'
                                    />
                                </div>
                            </Col>
                            <Col>
                                <ButtonToggle
                                    color="success"
                                    title='Buscar'
                                    size="sm"
                                    style={{ marginTop: '10px' }}
                                    onClick={() => {
                                        if (pokeBusqueda) {
                                            obtenerPokemon()
                                        }
                                    }}>
                                    <FaSearch />

                                </ButtonToggle>
                            </Col>
                        </Row>
                        <br />
                        <div className='pokeContainer'>
                            {listaPokemon ?
                                <PokeCards
                                    poke_imagen={listaPokemon.sprites.front_default}
                                    poke_nombre={listaPokemon.name}
                                    poke_habilidades={listaPokemon.abilities}
                                />
                                : ""}
                        </div>
                    </div>

                </Container>
            </div>
        </Fragment>
    )
}

export default PokeApi