import logo from './logo.svg';
import './App.css';
import PokeApi from './pokeApi/pokeApi';
import { Button } from 'reactstrap';

function App() {
  return (
    <PokeApi/>
  );
}

export default App;
